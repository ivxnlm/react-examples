import React from 'react';
import './Task.css'

class Task extends React.Component{


    styleParrafo(){
        return {
            fontSize: '20px',
            color: this.props.task.done ? 'yellow' : 'gray',
            textDecoration: 'none'
        }
    }

    render(){
        //Extraemos el objeto para que no escribamos mucho
        const {task} = this.props;
        return (
           <div className="red">
                <p style={this.styleParrafo()}>{task.id} {task.title} {task.description} {task.done}</p>
                <input type="checkbox"/>
                <button style={btnStyle}>X</button>
           </div>
        )
    }
}

const btnStyle = {
    fontSize : '16px',
    background: '#ff2027',
    color: '#000',
    border: 'none',
    padding: '10px 15px',
    borderRadius:'50%',
    cursor:'pointer'
}

export default Task;